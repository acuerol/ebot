#include "soc-hw.h"

uart_t   *uart0  = (uart_t *)    0x30000000;
timer_t  *timer0 = (timer_t *)   0x40000000;
gpio_t   *gpio0  = (gpio_t *)    0x50000000;

// Añadidos
esp_t          *esp0          = (esp_t *)          0x60000000;
motorL_t       *motorL0       = (motorL_t *)       0x70000000;
motorR_t       *motorR0       = (motorR_t *)       0x80000000;
ultrasonic_t   *ultrasonic0   = (ultrasonic_t *)   0x90000000;
led_t          *led0          = (led_t *)          0xA0000000;

isr_ptr_t isr_table[32];

void tic_isr();
/***************************************************************************
 * IRQ handling
 */
void isr_null()
{
}

void irq_handler(uint32_t pending)
{
	int i;

	for(i=0; i<32; i++) {
		if (pending & 0x01) (*isr_table[i])();
		pending >>= 1;
	}
}

void isr_init()
{
	int i;
	for(i=0; i<32; i++)
		isr_table[i] = &isr_null;
}

void isr_register(int irq, isr_ptr_t isr)
{
	isr_table[irq] = isr;
}

void isr_unregister(int irq)
{
	isr_table[irq] = &isr_null;
}

/***************************************************************************
 * General Functions
 */

short pow(short x, short y)
{
  short i = 0;
  if(y == 0)
    return 1;
  else
    for(i = 0; i <= y; i++)
	    x *= x;
  return x;
}

/***************************************************************************
 * Timer Functions
 */

void msleep(uint32_t msec)
{
	uint32_t tcr;

	// Use timer0.1
	timer0->compare1 = (FCPU/1000)*msec;
	timer0->counter1 = 0;
	timer0->tcr1 = TIMER_EN;

	do {
		//halt();
 		tcr = timer0->tcr1;
 	} while ( ! (tcr & TIMER_TRIG) );
}

void usleep(uint32_t usec)
{
	uint32_t tcr;

	// Use timer0.1
	timer0->compare1 = (FCPU/1000000)*usec;
	timer0->counter1 = 0;
	timer0->tcr1 = TIMER_EN;

	do {
		//halt();
 		tcr = timer0->tcr1;
 	} while ( ! (tcr & TIMER_TRIG) );
}

void nsleep(uint32_t nsec)
{
	uint32_t tcr;

	// Use timer0.1
	timer0->compare1 = (FCPU/1000000)*nsec;
	timer0->counter1 = 0;
	timer0->tcr1 = TIMER_EN;

	do {
		//halt();
 		tcr = timer0->tcr1;
 	} while ( ! (tcr & TIMER_TRIG) );
}

uint32_t tic_msec;

void tic_isr()
{
	tic_msec++;
	timer0->tcr0     = TIMER_EN | TIMER_AR | TIMER_IRQEN;
}

void tic_init()
{
	tic_msec = 0;

	// Setup timer0.0
	timer0->compare0 = (FCPU/10000);
	timer0->counter0 = 0;
	timer0->tcr0     = TIMER_EN | TIMER_AR | TIMER_IRQEN;

	isr_register(1, &tic_isr);
}

void run_timer(uint32_t  msec) {
	timer0->compare1 = (FCPU/1000)*msec;
	timer0->counter1 = 0;
	timer0->tcr1 = TIMER_EN;
}

short check_timer() {
  uint32_t tcr;
  
  tcr = timer0->tcr1;
 	return !(tcr & TIMER_TRIG);
}

/***************************************************************************
 * UART Functions
 */

char uart_getchar() {   
	while (! (uart0->ucr & UART_DR)) ;
	return uart0->rxtx;
}

void uart_putchar(char c) {
	while (uart0->ucr & UART_BUSY) ;
	uart0->rxtx = c;
}

void uart_putstr(char *str) {
	char *c = str;
	while(*c) {
		uart_putchar(*c);
		c++;
	}
}

/***************************************************************************
 * ESP8266 Functions
 */
char esp_rd_gpio0() {
   return esp0 -> gpio0;
}

char esp_rd_gpio2() {
   return esp0 -> gpio2;
}

void esp_wr_gpio(short out) {
   esp0 -> gpio_o = out;
}

// 1 = out
void esp_set_gpio_adr(short adr) {
   esp0 -> gpio_adr = adr;
}

void esp_rst() {
   esp0 -> mod_rst = 0;
}

/***************************************************************************
 * Motors
 */
void motorL_way(char way){
	motorL0 -> way = way;
}

void motorL_speed(short speed){
	motorL0 -> speed = speed;
}

void motorL_prog(char way, short speed){
	motorL_way(way);
	motorL_speed(speed);
}

void motorR_way(char way){
	motorR0 -> way = way;
}

void motorR_speed(short speed){
	motorR0 -> speed = speed;
}

void motorR_prog(char way, short speed){
	motorR_way(way);
	motorR_speed(speed);
}

void RLmotor(char way1, short speed1, char way2, short speed2) {
  motorL_way(way1);
	motorL_speed(speed1);
	motorR_way(way2);
	motorR_speed(speed2);
}

/***************************************************************************
 * Ultrasonic HC-SR04 Functions
 */
void ultrasonic_pulse() {
  ultrasonic0 -> trigger = 1;
}

char ultrasonic_read() {
  while(ultrasonic0 -> busy);
  return ultrasonic0 -> time;
}

short ultrasonic_busy() {
  return ultrasonic0 -> busy;
}

short measure() {
  ultrasonic_pulse();
  usleep(40);
  return ultrasonic_read();
}

/***************************************************************************
 * Leds Functions
 */

void print_led(char num){
	led0 -> send = num;
} 

char read_led() {
	return led0 -> read;
}

char led_state(char num) {
  char leds = read_led();
  leds = leds >> num;
  return leds % 2;
}

void on_led(short led){
	led0 -> led_on = led;   // Añadido FB
}

void off_led(short led){
	led0 -> led_off = led;  // Añadido FB
}

void led_ascend() {
  short i = 0;
  short num = 1;
  print_led(0);
  for(i = 0 ; i < 8 ; i ++) {
    print_led(num);
    msleep(50);
    num *= 2;
  }
  print_led(0);
}

void led_descend() {
  short i = 0;
  short num = 128;
  print_led(0);
  for(i = 0 ; i < 8 ; i ++) {
    print_led(num);
    msleep(50);
    num /= 2;
  }
  print_led(0);
}

/***************************************************************************
 * General Porpouse
 */
