#ifndef SPIKEHW_H
#define SPIKEHW_H

#define PROMSTART 0x00000000
#define RAMSTART  0x00000800
#define RAMSIZE   0x100
#define RAMEND    (RAMSTART + RAMSIZE)

#define RAM_START 0x40000000
#define RAM_SIZE  0x04000000

#define FCPU      50000000

#define UART_RXBUFSIZE 32

/****************************************************************************
 * Types
 */
typedef unsigned int  uint32_t;    // 32 Bit
typedef signed   int   int32_t;    // 32 Bit

typedef unsigned char  uint8_t;    // 8 Bit
typedef signed   char   int8_t;    // 8 Bit

/****************************************************************************
 * Interrupt handling
 */
typedef void(*isr_ptr_t)(void);

void     irq_enable();
void     irq_disable();
void     irq_set_mask(uint32_t mask);
uint32_t irq_get_mak();

void     isr_init();
void     isr_register(int irq, isr_ptr_t isr);
void     isr_unregister(int irq);

/****************************************************************************
 * General Stuff
 */
void     halt();
void     jump(uint32_t addr);


/****************************************************************************
 * Timer
 */
#define TIMER_EN     0x08    // Enable Timer
#define TIMER_AR     0x04    // Auto-Reload
#define TIMER_IRQEN  0x02    // IRQ Enable
#define TIMER_TRIG   0x01    // Triggered (reset when writing to TCR)

typedef struct {
	volatile uint32_t tcr0;
	volatile uint32_t compare0;
	volatile uint32_t counter0;
	volatile uint32_t tcr1;
	volatile uint32_t compare1;
	volatile uint32_t counter1;
} timer_t;

void msleep(uint32_t msec);
void nsleep(uint32_t nsec);

void tic_init();

void run_timer(uint32_t msec);
short check_timer();

/***************************************************************************
 * GPIO0
 */
typedef struct {
	volatile uint32_t ctrl;
	volatile uint32_t dummy1;
	volatile uint32_t dummy2;
	volatile uint32_t dummy3;
	volatile uint32_t in;
	volatile uint32_t out;
	volatile uint32_t oe;
} gpio_t;

/***************************************************************************
 * UART0
 */
#define UART_DR   0x01                    // RX Data Ready
#define UART_ERR  0x02                    // RX Error
#define UART_BUSY 0x10                    // TX Busy

typedef struct {
   volatile uint32_t ucr;
   volatile uint32_t rxtx;
} uart_t;

void uart_init();
void uart_putchar(char c);
void uart_putstr(char *str);
char uart_getchar();

/***************************************************************************
 * ESP0
 */
typedef struct {
   volatile short gpio0;       // 0
   volatile short gpio2;       // 2
   volatile short gpio_o;     // 4
   volatile short gpio_adr;   // 6
   volatile short mod_rst;     // 8
} esp_t;

char esp_rd_gpio0();
char esp_rd_gpio2();
void esp_wr_gpio(short out);
void esp_set_gpio_adr(short adr);
void esp_rst();

/***************************************************************************
 * MotorL0
 */
typedef struct {
   volatile char way;       // 0
   volatile short speed;       // 1
} motorL_t;

void motorL_way(char way);
void motorL_speed(short speed);
void motorL_prog(char way, short speed);

/***************************************************************************
 * MotorR0
 */
typedef struct {
   volatile char way;       // 0
   volatile short speed;       // 1
} motorR_t;

void motorR_way(char way);
void motorR_speed(short speed);
void motorR_prog(char way, short speed);

/***************************************************************************
 * ULTRASONIC0
 */
typedef struct {
   volatile short trigger;
   volatile short busy;
//   volatile short testtttts;
   volatile uint32_t time;
} ultrasonic_t;

void ultrasonic_pulse();
char ultrasonic_read();
short ultrasonic_busy();
short measure();

/***************************************************************************
 * Leds0
 */
typedef struct {
   volatile char send;
   volatile char read;
   volatile char led_on;
   volatile char led_off;
} led_t;

void print_led(char num);
char read_led();
char led_state(char num);
void on_led(short led);
void off_led(short led);
void led_ascend();
void led_descend();


/***************************************************************************
 * ADC
 */
void init_adc();
short read_adc();

/***************************************************************************
 * Pointer to actual components
 */
extern timer_t  *timer0;
extern uart_t   *uart0; 
extern gpio_t   *gpio0; 
extern uint32_t *sram0; 

extern esp_t *esp0;
extern motorL_t *motorL0;
extern motorR_t *motorR0;
extern ultrasonic_t *ultrasonic0;
extern led_t *led0;

#endif // SPIKEHW_H
