#include "soc-hw.h"

#define BUFFER_SIZE 50
#define BUFFERC_SIZE 20
uint32_t buffer[BUFFER_SIZE];
uint32_t bufferT[BUFFER_SIZE];

uint32_t bufferC[BUFFERC_SIZE];

/*------------------------------------------
String
*/

short length(char *str) {
    short length = 0;
    char *c = str;
    while(*c) {
      length++;
      c++;
    }
    return length;
}

short length_buffer() {
    short i = 0;
    while(buffer[i]) {
      i++;
    }
    return i;
}

short length_bufferC() {
    short i = 0;
    while(bufferC[i]) {
      i++;
    }
    return i;
}

char charAt(short pos) {
  if(pos <= length_buffer()) {
    return buffer[pos];
  } else
    return -1;
}

char compare(char *str2) {
  short len1 = length_buffer();
  short len2 = length(str2);

  char *c2 = str2;

  short i = 0;
  if(len1 == len2) {
    for(i = 0 ; i <= len1 ; i++) {
      if(buffer[i] != *(c2 + i))
        return 0;
    }
    return 1;
  } else 
    return 0;
}

char contains(char *str2) {
  short len1 = length_buffer();
  short len2 = length(str2);

  char *c2 = str2;
  
  short i = 0;
  short j = 0;
  
  if(len1 > len2) {
    for(i = 0 ; i <= len1 ; i++) {
       if(buffer[i] == *(c2 + j)) {
        j++;
        if(j == len2)
          return 1;
       } else
        j = 0;
    }
    return 0;
  } else
    if(len1 == len2)
      return compare(str2);
    else
      return 0;
}

char firstIndexOf(char *str2, short ini) {
  short len1 = length_buffer();
  short len2 = length(str2);

  char *c2 = str2;
  
  short i = 0;
  short j = 0;
  
  char index = 0;
  
  if(len1 > len2) {
    for(i = ini ; i <= len1 ; i++) {
       if(buffer[i] == *(c2 + j)) {
        // Si es igual el primero guarda la posición en el string original.
        if(j == 0)
          index = i;
        j++;
        if(j == len2)
          return index;
       } else
        j = 0;
    }
    return -1;
  } else
    if(len1 == len2)
      if(compare(str2))
        return 0;
      else
        return -1;
    else
      return -1;
}

short lastIndexOf(char *str2, short ini) {
  short len1 = length_buffer();
  short len2 = length(str2);

  char *c2 = str2;
  
  short i = 0;
  short j = len2 - 1;
  
  if(ini < len1)
    if(len1 > len2) {
      for(i = ini ; i >= 0 ; i--) {
        if(buffer[i] == *(c2 + j)) {
          j--;
          if(j < 0)
            return i;
        } else
          j = len2 - 1;
      }
      return -1;
    } else
      if(len1 == len2)
        if(compare(str2))
          return 0;
        else
          return -1;
      else
        return -1;
  else
    return -1;
}

void substring(char *str, char ini, char end) {
  char *c = str;  
  char i = 0;
  char j = 0;
  
  for(i = ini ; i < end ; i++) {
    buffer[j] = *(c + i);
    j++;
  }
  
   buffer[j] = '\0';
}

void substring_buffer(char ini, char end) {
  char i = 0;
  char j = 0;
  
  for(i = ini ; i < end ; i++) {
    bufferT[j] = buffer[i];
    j++;
  }
  
   bufferT[j] = '\0';
}

void substring_bufferT(char ini, char end) {
  char i = 0;
  char j = 0;
  
  for(i = ini ; i < end ; i++) {
    buffer[j] = bufferT[i];
    j++;
  }
  
   buffer[j] = '\0';
}

void print_buffer() {
  short i = 0;
  for(i = 0 ; i < BUFFER_SIZE ; i++) {
    if(buffer[i] != '\0')
      uart_putchar(buffer[i]);
    else
      break;
  }
}

void print_bufferT() {
  short i = 0;
  for(i = 0 ; i < BUFFER_SIZE ; i++) {
    if(bufferT[i] != '\0')
      uart_putchar(bufferT[i]);
    else
      break;
  }
}

void print_bufferC() {
  short i = 0;
  for(i = 0 ; i < BUFFERC_SIZE ; i++) {
    if(bufferC[i] != '\0')
      uart_putchar(bufferC[i]);
    else
      break;
  }
  uart_putchar(0x0d);
  uart_putchar(0x0a);
}

void read_string(short wait_msec) {
  short i = 0;
  run_timer(wait_msec);
  do {
    if(uart0->ucr & UART_DR) {
      buffer[i] = uart_getchar();
      run_timer(wait_msec);
      i++;
    }
  } while(check_timer());
  buffer[i] = '\0';
}

void copy_buffer(short to) {
  short i = 0;
  if(to == 1)
    for(i = 0 ; i < BUFFER_SIZE ; i++)
      buffer[i] = bufferT[i];
  else if(to == 2)
    for(i = 0 ; i < BUFFER_SIZE ; i++)
      bufferT[i] = buffer[i];
  else if(to == 3)
    for(i = 0 ; i < BUFFERC_SIZE ; i++)
      bufferC[i] = bufferT[i];
}

void str2buf(char *str) {
  char *c = str;  
  char i = 0;

  while(*c) {
    buffer[i] = *c;
    i++;
    c++;
  }
  
  buffer[i] = '\0';
}

void str2bufC(char *str) {
  char *c = str;  
  char i = 0;

  while(*c) {
    bufferC[i] = *c;
    i++;
    c++;
  }
  
  bufferC[i] = '\0';
}

void concat(char c) {
  char i = length_bufferC();

  bufferC[i] = c;
  bufferC[i+1] = '\0';
}

short char2number(char in) {
  switch(in) {
  case '0':
    return 0;
  case '1':
    return 1;
  case '2':
    return 2;
  case '3':
    return 3;
  case '4':
    return 4;
  case '5':
    return 5;
  case '6':
    return 6;
  case '7':
    return 7;
  case '8':
    return 8;
  case '9':
    return 9;
  default:
    return -1;
  }
}

short charToShortNumber(char init, char end){  // Toma un número en caracteres de más de un dígito y lo transforma a un short
	short number = 0;
	short factor = 0;
	short power = 0;
	short i = 0;
	power = 1;
	
	for(i = end; i >= init; i--){
		factor = (char2number(buffer[i]))*power;
		number += factor;
		power = power*10;
	}
	return number;
}

void conditionDecoderMotorL(short distance, char conditional, short speed, char way){
	switch(conditional){
		case '<':
			if((ultrasonic_read()/58)<distance){
			motorL_prog(way, speed);
			} else {
			motorL_prog(0, 0);
			}
			break;
		case '>':
			if((ultrasonic_read()/58)>distance){
			motorL_prog(way, speed);
			} else {
			motorL_prog(0, 0);
			}
			break;
		case '=':
			if((ultrasonic_read()/58)==distance){
			motorL_prog(way, speed);
			} else {
			motorL_prog(0, 0);
			}
			break;
		default:
			motorL_prog(0, 0);
	}
}

void conditionDecoderMotorR(short distance, char conditional, short speed, char way){
	switch(conditional){
		case '<':
			if((ultrasonic_read()/58)<distance){
			motorR_prog(way, speed);
			} else {
			motorR_prog(0, 0);
			}
			break;
		case '>':
			if((ultrasonic_read()/58)>distance){
			motorR_prog(way, speed);
			} else {
			motorR_prog(0, 0);
			}
			break;
		case '=':
			if((ultrasonic_read()/58)==distance){
			motorR_prog(way, speed);
			} else {
			motorR_prog(0, 0);
			}
			break;
		default:
			motorR_prog(0, 0);
	}
}

void conditionDecoderLedOn(short distance, char conditional, short led){
	switch(conditional){
		case '<':
			if((ultrasonic_read()/58)<distance){
			on_led(led);
			} else {
			print_led(0);
			}
			break;
		case '>':
			if((ultrasonic_read()/58)<distance){
			on_led(led);
			} else {
			print_led(0);
			}
			break;
		case '=':
			if((ultrasonic_read()/58)<distance){
			on_led(led);
			} else {
			print_led(0);
			}
			break;
		default:
			on_led(0);
	}
}

void conditionDecoderLedOff(short distance, char conditional, short led){
	switch(conditional){
		case '<':
			if((ultrasonic_read()/58)<distance){
			off_led(led);
			} else {
			print_led(255);
			}
			break;
		case '>':
			if((ultrasonic_read()/58)<distance){
			off_led(led);
			} else {
			print_led(255);
			}
			break;
		case '=':
			if((ultrasonic_read()/58)<distance){
			off_led(led);
			} else {
			print_led(255);
			}
			break;
		default:
			off_led(0);
	}
}

void actionDecoder(short distance, char conditional){
	short pos= 0;
	short initAct= 0;
	short speed = 0;
	char way = 0;
	short led = 0;
	char led_state = 0;
	if(contains("motorL")==1){
		initAct = firstIndexOf("motorL", 0);
		pos = firstIndexOf(",", initAct);  
		if((pos-1)!=(7+initAct)){
			speed = charToShortNumber(7+initAct, pos-1);
		} else {
			speed = char2number(buffer[7+initAct]);
		}  
		way = buffer[pos+1];  
		
		conditionDecoderMotorL(distance, conditional, speed, way);
	} 
	else if(contains("motorR")==1){// sintaxis: motorR(speed[2 caracteres],way[1]
		initAct = firstIndexOf("motorR", 0);
		pos = firstIndexOf(",", initAct);  // Ubica la primera coma de la orden
		if((pos-1)!=(7+initAct)){
			speed = charToShortNumber(7+initAct, pos-1);
		} else {
			speed = char2number(buffer[7+initAct]);
		} 
		way = buffer[pos+1];  
		conditionDecoderMotorR(distance, conditional, speed, way);
	}
	else if(contains("led")==1){
		initAct = firstIndexOf("led", 0);
		led = char2number(buffer[4+initAct]);    // sintaxis: led(led[1 caracter],1/0)
		led_state = buffer[6+initAct];
		if(led_state == '0'){
			conditionDecoderLedOn(distance, conditional, led);
		} else if(led_state == '1'){
			conditionDecoderLedOff(distance, conditional, led);
		}
	}
}

void orderDecoder(){
	short pos= 0;
	short speed = 0;
	char way = 0;
	short led = 0;
	char led_state = 0;
	short distance = 0;
	char conditional = 0;
	if(contains("ultra")==1){  // sintaxis: ultra(distance[3 caracteres],condición[1 caracter],acción)
		pos = firstIndexOf(",", 0);
		if((pos-1)!=6){
			distance = charToShortNumber(6, pos-1);
		} else {
			distance = char2number(buffer[6]);
		}
		conditional = buffer[pos+1];
		actionDecoder(distance, conditional);
	}
	else if(contains("motorL")==1){// sintaxis: motorL(speed[2 caracteres],way[1]
		pos = firstIndexOf(",", 0);  // Ubica la primera coma de la orden
		if((pos-1)!=7){
			speed = charToShortNumber(7, pos-1);
		} else {
			speed = char2number(buffer[7]);
		}  
		way = buffer[pos+1];  //dos espacios adelnte de la coma incluyendo el espacio
		motorL_prog(way, speed);
	} 
	else if(contains("motorR")==1){// sintaxis: motorR(speed[2 caracteres],way[1]
		pos = firstIndexOf(",", 0);  // Ubica la primera coma de la orden
		if((pos-1)!=7){
			speed = charToShortNumber(7, pos-1);
		} else {
			speed = char2number(buffer[7]);
		} 
		way = buffer[pos+1];  
		motorR_prog(way, speed);
	}
	else if(contains("led")==1){
		led = char2number(buffer[4]);    // sintaxis: led(led[1 caracter],1/0)
		led_state = buffer[6];
		if(led_state == '0'){
			off_led(led);
		} else if(led_state == '1'){
			on_led(led);
		}
	}
}
void send_ultrasonic() {
  short dist = measure();
  uart_putstr("ultrasonic=");
  uart_putchar(dist);
  uart_putchar(0x0d);
  uart_putchar(0x0a);
}

int main()
{ 	
/*------------------------------------------
Test
*/
  uart_putstr("** Start Program eBot**\n");
  esp_set_gpio_adr(0);
  
  led_ascend();
  led_descend();
  
  while(1) {
    //send_ultrasonic();    
    read_string(10);
    orderDecoder();
  }
  
  uart_putstr("** End Program eBot**\n");
  
  return 0;
}
