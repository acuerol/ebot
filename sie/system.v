//---------------------------------------------------------------------------
// LatticeMico32 System On A Chip
//
// Top Level Design for the Xilinx Spartan 3-200 Starter Kit
//---------------------------------------------------------------------------

module system
#(
//	parameter   bootram_file     = "../firmware/cain_loader/image.ram",
//	parameter   bootram_file     = "../firmware/arch_examples/image.ram",
//	parameter   bootram_file     = "../firmware/boot0-serial/image.ram",
   parameter   bootram_file     = "../firmware/hw-test/image.ram",
	parameter   clk_freq         = 50000000,
	parameter   uart_baud_rate   = 9600
) (
	input             clk, 
	// Debug 
	output            led,
	input             rst_i,
	
	// UART
	input             uart_rxd, 
	output            uart_txd,
	
	// debug
	//input             uart_rxdD, 
	//output            uart_txdD,
	
   // CPU Interface
   input             [12:0] addr,
   input             [7:0]  sram_data,
   input             nwe,
   input             noe,
   input             ncs,

   // ESP8266 Driver
   output rst_esp,
   inout [1:0] gpio_esp,
   
   //Motor L 
   output pwmL_forward,
   output pwmL_backward,
   
   //Motor R 
   output pwmR_forward,
   output pwmR_backward,


   // Ultrasonic
   input echo,
   output trigger,

   // Leds
   output [7:0] number_o
);
	
wire rst;

assign rst = rst_i;
	
//------------------------------------------------------------------
// Whishbone Wires
//------------------------------------------------------------------
wire         gnd   =  1'b0;
wire   [3:0] gnd4  =  4'h0;
wire  [31:0] gnd32 = 32'h00000000;

 
wire [31:0]  lm32i_adr,
             lm32d_adr,
             uart0_adr,
             timer0_adr,
             gpio0_adr,
             bram0_adr,
             esp0_adr,
             motorL0_adr,
             motorR0_adr,
             ultrasonic0_adr,
             led0_adr;


wire [31:0]  lm32i_dat_r,
             lm32i_dat_w,
             lm32d_dat_r,
             lm32d_dat_w,
             uart0_dat_r,
             uart0_dat_w,
             timer0_dat_r,
             timer0_dat_w,
             gpio0_dat_r,
             gpio0_dat_w,
             bram0_dat_r,
             bram0_dat_w,
             sram0_dat_w,
             sram0_dat_r,
             
             esp0_dat_w,
             esp0_dat_r,
             motorL0_dat_w,
             motorL0_dat_r,
             motorR0_dat_w,
             motorR0_dat_r,
             ultrasonic0_dat_w,
             ultrasonic0_dat_r,
             led0_dat_w,
             led0_dat_r;

wire [3:0]   lm32i_sel,
             lm32d_sel,
             uart0_sel,
             timer0_sel,
             gpio0_sel,
             bram0_sel,
             sram0_sel,
             
             esp0_sel,
             motorL0_sel,
             motorR0_sel,
             ultrasonic0_sel,
             led0_sel;

wire         lm32i_we,
             lm32d_we,
             uart0_we,
             timer0_we,
             gpio0_we,
             bram0_we,
             sram0_we,
             
             esp0_we,
             motorL0_we,
             motorR0_we,
             ultrasonic0_we,
             led0_we;
             
wire         lm32i_cyc,
             lm32d_cyc,
             uart0_cyc,
             timer0_cyc,
             gpio0_cyc,
             bram0_cyc,
             sram0_cyc,
             
             esp0_cyc,
             motorL0_cyc,
             motorR0_cyc,
             ultrasonic0_cyc,
             led0_cyc;

wire         lm32i_stb,
             lm32d_stb,
             uart0_stb,
             timer0_stb,
             gpio0_stb,
             bram0_stb,
             sram0_stb,
             
             esp0_stb,
             motorL0_stb,
             motorR0_stb,
             ultrasonic0_stb,
             led0_stb;

wire         lm32i_ack,
             lm32d_ack,
             uart0_ack,
             timer0_ack,
             gpio0_ack,
             bram0_ack,
             sram0_ack,
             
             esp0_ack,
             motorL0_ack,
             motorR0_ack,
             ultrasonic0_ack,
             led0_ack;

wire         lm32i_rty,
             lm32d_rty;

wire         lm32i_err,
             lm32d_err;

wire         lm32i_lock,
             lm32d_lock;

wire [2:0]   lm32i_cti,
             lm32d_cti;

wire [1:0]   lm32i_bte,
             lm32d_bte;

//---------------------------------------------------------------------------
// Interrupts
//---------------------------------------------------------------------------
wire [31:0]  intr_n;
wire         uart0_intr = 0;
wire   [1:0] timer0_intr;
wire         gpio0_intr;

assign intr_n = { 28'hFFFFFFF, ~timer0_intr[1], ~gpio0_intr, ~timer0_intr[0], ~uart0_intr };

/*
debouncer deb (
.clk(clk),
.btn_sig(rst_i),
.sig_out(rst)
);
*/
//---------------------------------------------------------------------------
// Wishbone Interconnect
//---------------------------------------------------------------------------
conbus #(
	.s_addr_w(4),
	.s0_addr(4'b0000),	// bram        0x00000000 
	.s1_addr(4'b0011),	// uart        0x30000000 
	.s2_addr(4'b0100),	// timer       0x40000000
	.s3_addr(4'b0101),	// gpio        0x50000000
	.s4_addr(4'b0110),	// esp         0x60000000
	.s5_addr(4'b0111),	// motorL      0x70000000
	.s6_addr(4'b1000),	// motorR      0x80000000
	.s7_addr(4'b1001),	// ultrasonic  0x90000000
	.s8_addr(4'b1010)	   // led        0xA0000000
) conbus0(
	.sys_clk( clk ),
	.sys_rst( ~rst ),
	// Master0
	.m0_dat_i(  lm32i_dat_w  ),
	.m0_dat_o(  lm32i_dat_r  ),
	.m0_adr_i(  lm32i_adr    ),
	.m0_we_i (  lm32i_we     ),
	.m0_sel_i(  lm32i_sel    ),
	.m0_cyc_i(  lm32i_cyc    ),
	.m0_stb_i(  lm32i_stb    ),
	.m0_ack_o(  lm32i_ack    ),
	// Master1
	.m1_dat_i(  lm32d_dat_w  ),
	.m1_dat_o(  lm32d_dat_r  ),
	.m1_adr_i(  lm32d_adr    ),
	.m1_we_i (  lm32d_we     ),
	.m1_sel_i(  lm32d_sel    ),
	.m1_cyc_i(  lm32d_cyc    ),
	.m1_stb_i(  lm32d_stb    ),
	.m1_ack_o(  lm32d_ack    ),
	// Master2
	.m2_dat_i(  gnd32  ),
	.m2_adr_i(  gnd32  ),
	.m2_sel_i(  gnd4   ),
	.m2_cyc_i(  gnd    ),
	.m2_stb_i(  gnd    ),
	// Master3
	.m3_dat_i(  gnd32  ),
	.m3_adr_i(  gnd32  ),
	.m3_sel_i(  gnd4   ),
	.m3_cyc_i(  gnd    ),
	.m3_stb_i(  gnd    ),
	// Master4
	.m4_dat_i(  gnd32  ),
	.m4_adr_i(  gnd32  ),
	.m4_sel_i(  gnd4   ),
	.m4_cyc_i(  gnd    ),
	.m4_stb_i(  gnd    ),
	// Master5
	.m5_dat_i(  gnd32  ),
	.m5_adr_i(  gnd32  ),
	.m5_sel_i(  gnd4   ),
	.m5_cyc_i(  gnd    ),
	.m5_stb_i(  gnd    ),
	// Master6
	.m6_dat_i(  gnd32  ),
	.m6_adr_i(  gnd32  ),
	.m6_sel_i(  gnd4   ),
	.m6_cyc_i(  gnd    ),
	.m6_stb_i(  gnd    ),


	// Slave0
	.s0_dat_i(  bram0_dat_r ),
	.s0_dat_o(  bram0_dat_w ),
	.s0_adr_o(  bram0_adr   ),
	.s0_sel_o(  bram0_sel   ),
	.s0_we_o(   bram0_we    ),
	.s0_cyc_o(  bram0_cyc   ),
	.s0_stb_o(  bram0_stb   ),
	.s0_ack_i(  bram0_ack   ),
	// Slave1
	.s1_dat_i(  uart0_dat_r ),
	.s1_dat_o(  uart0_dat_w ),
	.s1_adr_o(  uart0_adr   ),
	.s1_sel_o(  uart0_sel   ),
	.s1_we_o(   uart0_we    ),
	.s1_cyc_o(  uart0_cyc   ),
	.s1_stb_o(  uart0_stb   ),
	.s1_ack_i(  uart0_ack   ),
	// Slave2
	.s2_dat_i(  timer0_dat_r ),
	.s2_dat_o(  timer0_dat_w ),
	.s2_adr_o(  timer0_adr   ),
	.s2_sel_o(  timer0_sel   ),
	.s2_we_o(   timer0_we    ),
	.s2_cyc_o(  timer0_cyc   ),
	.s2_stb_o(  timer0_stb   ),
	.s2_ack_i(  timer0_ack   ),
	// Slave3
	.s3_dat_i(  gpio0_dat_r ),
	.s3_dat_o(  gpio0_dat_w ),
	.s3_adr_o(  gpio0_adr   ),
	.s3_sel_o(  gpio0_sel   ),
	.s3_we_o(   gpio0_we    ),
	.s3_cyc_o(  gpio0_cyc   ),
	.s3_stb_o(  gpio0_stb   ),
	.s3_ack_i(  gpio0_ack   ),
	
	// Slave4 esp
	.s4_dat_i(  esp0_dat_r ),
	.s4_dat_o(  esp0_dat_w ),
	.s4_adr_o(  esp0_adr   ),
	.s4_sel_o(  esp0_sel   ),
	.s4_we_o(   esp0_we    ),
	.s4_cyc_o(  esp0_cyc   ),
	.s4_stb_o(  esp0_stb   ),
	.s4_ack_i(  esp0_ack   ),
	
	// Slave5 MotorL
	.s5_dat_i(  motorL0_dat_r ),
	.s5_dat_o(  motorL0_dat_w ),
	.s5_adr_o(  motorL0_adr   ),
	.s5_sel_o(  motorL0_sel   ),
	.s5_we_o(   motorL0_we    ),
	.s5_cyc_o(  motorL0_cyc   ),
	.s5_stb_o(  motorL0_stb   ),
	.s5_ack_i(  motorL0_ack   ),
	
	// Slave6 MotorR
	.s6_dat_i(  motorR0_dat_r ),
	.s6_dat_o(  motorR0_dat_w ),
	.s6_adr_o(  motorR0_adr   ),
	.s6_sel_o(  motorR0_sel   ),
	.s6_we_o(   motorR0_we    ),
	.s6_cyc_o(  motorR0_cyc   ),
	.s6_stb_o(  motorR0_stb   ),
	.s6_ack_i(  motorR0_ack   ),
	
	// Slave7 Ultrasonic
	.s7_dat_i(  ultrasonic0_dat_r ),
	.s7_dat_o(  ultrasonic0_dat_w ),
	.s7_adr_o(  ultrasonic0_adr   ),
	.s7_sel_o(  ultrasonic0_sel   ),
	.s7_we_o(   ultrasonic0_we    ),
	.s7_cyc_o(  ultrasonic0_cyc   ),
	.s7_stb_o(  ultrasonic0_stb   ),
	.s7_ack_i(  ultrasonic0_ack   ),
	
	// Slave8 Led
	.s8_dat_i(  led0_dat_r ),
	.s8_dat_o(  led0_dat_w ),
	.s8_adr_o(  led0_adr   ),
	.s8_sel_o(  led0_sel   ),
	.s8_we_o(   led0_we    ),
	.s8_cyc_o(  led0_cyc   ),
	.s8_stb_o(  led0_stb   ),
	.s8_ack_i(  led0_ack   )
);


//---------------------------------------------------------------------------
// LM32 CPU 
//---------------------------------------------------------------------------
lm32_cpu lm0 (
	.clk_i(  clk  ),
	.rst_i(  ~rst  ),
	.interrupt_n(  intr_n  ),
	//
	.I_ADR_O(  lm32i_adr    ),
	.I_DAT_I(  lm32i_dat_r  ),
	.I_DAT_O(  lm32i_dat_w  ),
	.I_SEL_O(  lm32i_sel    ),
	.I_CYC_O(  lm32i_cyc    ),
	.I_STB_O(  lm32i_stb    ),
	.I_ACK_I(  lm32i_ack    ),
	.I_WE_O (  lm32i_we     ),
	.I_CTI_O(  lm32i_cti    ),
	.I_LOCK_O( lm32i_lock   ),
	.I_BTE_O(  lm32i_bte    ),
	.I_ERR_I(  lm32i_err    ),
	.I_RTY_I(  lm32i_rty    ),
	//
	.D_ADR_O(  lm32d_adr    ),
	.D_DAT_I(  lm32d_dat_r  ),
	.D_DAT_O(  lm32d_dat_w  ),
	.D_SEL_O(  lm32d_sel    ),
	.D_CYC_O(  lm32d_cyc    ),
	.D_STB_O(  lm32d_stb    ),
	.D_ACK_I(  lm32d_ack    ),
	.D_WE_O (  lm32d_we     ),
	.D_CTI_O(  lm32d_cti    ),
	.D_LOCK_O( lm32d_lock   ),
	.D_BTE_O(  lm32d_bte    ),
	.D_ERR_I(  lm32d_err    ),
	.D_RTY_I(  lm32d_rty    )
);
	
//---------------------------------------------------------------------------
// Block RAM
//---------------------------------------------------------------------------
wb_bram #(
	.adr_width( 13 ),
	.mem_file_name( bootram_file )
) bram0 (
	.clk_i(  clk  ),
	.rst_i(  ~rst  ),
	//
	.wb_adr_i(  bram0_adr    ),
	.wb_dat_o(  bram0_dat_r  ),
	.wb_dat_i(  bram0_dat_w  ),
	.wb_sel_i(  bram0_sel    ),
	.wb_stb_i(  bram0_stb    ),
	.wb_cyc_i(  bram0_cyc    ),
	.wb_ack_o(  bram0_ack    ),
	.wb_we_i(   bram0_we     )
);

//---------------------------------------------------------------------------
// uart0
//---------------------------------------------------------------------------
wire uart0_rxd;
wire uart0_txd;

wb_uart #(
	.clk_freq( clk_freq        ),
	.baud(     uart_baud_rate  )
) uart0 (
	.clk( clk ),
	.reset( ~rst ),
	//
	.wb_adr_i( uart0_adr ),
	.wb_dat_i( uart0_dat_w ),
	.wb_dat_o( uart0_dat_r ),
	.wb_stb_i( uart0_stb ),
	.wb_cyc_i( uart0_cyc ),
	.wb_we_i(  uart0_we ),
	.wb_sel_i( uart0_sel ),
	.wb_ack_o( uart0_ack ), 
//	.intr(       uart0_intr ),
	.uart_rxd( uart0_rxd ),
	.uart_txd( uart0_txd )
);

//---------------------------------------------------------------------------
// timer0
//---------------------------------------------------------------------------
wb_timer #(
	.clk_freq(   clk_freq  )
) timer0 (
	.clk(      clk          ),
	.reset(    ~rst          ),
	//
	.wb_adr_i( timer0_adr   ),
	.wb_dat_i( timer0_dat_w ),
	.wb_dat_o( timer0_dat_r ),
	.wb_stb_i( timer0_stb   ),
	.wb_cyc_i( timer0_cyc   ),
	.wb_we_i(  timer0_we    ),
	.wb_sel_i( timer0_sel   ),
	.wb_ack_o( timer0_ack   ), 
	.intr(     timer0_intr  )
);

//---------------------------------------------------------------------------
// General Purpose IO
//---------------------------------------------------------------------------

wire [7:0] gpio0_io;
wire        gpio0_irq;

wb_gpio gpio0 (
	.clk(      clk          ),
	.rst(    ~rst          ),
	//
	.wb_adr_i( gpio0_adr    ),
	.wb_dat_i( gpio0_dat_w  ),
	.wb_dat_o( gpio0_dat_r  ),
	.wb_stb_i( gpio0_stb    ),
	.wb_cyc_i( gpio0_cyc    ),
	.wb_we_i(  gpio0_we     ),
	.wb_ack_o( gpio0_ack    ), 
	// GPIO
	.gpio_io(gpio0_io)
);

//---------------------------------------------------------------------------
// ESP8266 Driver
//---------------------------------------------------------------------------

wb_esp #(
   .clk_freq( clk_freq )
)  esp0 ( .clk( clk ),
	.rst( ~rst ),
	//
	.wb_adr_i( esp0_adr    ),
	.wb_dat_i( esp0_dat_w  ),
	.wb_dat_o( esp0_dat_r  ),
	.wb_stb_i( esp0_stb    ),
	.wb_cyc_i( esp0_cyc    ),
	.wb_we_i(  esp0_we     ),
	.wb_ack_o( esp0_ack    ), 
	// GPIO
	.rst_esp(rst_esp),
	.gpio_io(gpio_esp)
);

// Motor Left Driver
//---------------------------------------------------------------------------

wb_motor #(
   .clk_freq( clk_freq )
)  motorL0 ( 
   .clk( clk ),
	.rst( ~rst ),
	//
	.wb_adr_i( motorL0_adr    ),
	.wb_dat_i( motorL0_dat_w  ),
	.wb_dat_o( motorL0_dat_r  ),
	.wb_stb_i( motorL0_stb    ),
	.wb_cyc_i( motorL0_cyc    ),
	.wb_we_i(  motorL0_we     ),
	.wb_sel_i( motorL0_sel    ),
	.wb_ack_o( motorL0_ack    ), 
	// Motor
	.pwm_forward( pwmL_forward ),
	.pwm_backward( pwmL_backward )
);

// Motor Right Driver
//---------------------------------------------------------------------------

wb_motor #(
   .clk_freq( clk_freq )
)  motorR0 ( 
   .clk( clk ),
	.rst( ~rst ),
	//
	.wb_adr_i( motorR0_adr    ),
	.wb_dat_i( motorR0_dat_w  ),
	.wb_dat_o( motorR0_dat_r  ),
	.wb_stb_i( motorR0_stb    ),
	.wb_cyc_i( motorR0_cyc    ),
	.wb_we_i(  motorR0_we     ),
	.wb_sel_i( motorR0_sel    ),
	.wb_ack_o( motorR0_ack    ), 
	// Motor
	.pwm_forward( pwmR_forward ),
	.pwm_backward( pwmR_backward )
);

//---------------------------------------------------------------------------
// Ultrasonic Driver
//---------------------------------------------------------------------------

wb_ultrasonic #(
   .clk_freq( clk_freq )
)  ultrasonic0 ( 
   .clk( clk ),
	.rst( ~rst ),
	//
	.wb_adr_i( ultrasonic0_adr    ),
	.wb_dat_i( ultrasonic0_dat_w  ),
	.wb_dat_o( ultrasonic0_dat_r  ),
	.wb_stb_i( ultrasonic0_stb    ),
	.wb_cyc_i( ultrasonic0_cyc    ),
	.wb_we_i(  ultrasonic0_we     ),
	.wb_sel_i( ultrasonic0_sel    ),
	.wb_ack_o( ultrasonic0_ack    ), 
	// Ultrasonic
	.echo( echo ),
	.trigger_o( trigger )
);

//---------------------------------------------------------------------------
// Led COntrol
//---------------------------------------------------------------------------

wb_led led0 ( .clk( clk ),
	.rst( ~rst ),
	//
	.wb_adr_i( led0_adr    ),
	.wb_dat_i( led0_dat_w  ),
	.wb_dat_o( led0_dat_r  ),
	.wb_stb_i( led0_stb    ),
	.wb_cyc_i( led0_cyc    ),
	.wb_we_i(  led0_we     ),
	.wb_sel_i( led0_sel ),
	.wb_ack_o( led0_ack    ), 

	
	// leds
	.number_o(number_o)
	
);

//----------------------------------------------------------------------------
// Mux UART wires according to sw[0]
//----------------------------------------------------------------------------
assign uart_txd  = uart0_txd;
assign uart0_rxd = uart_rxd;
assign led       = ~uart_txd;
endmodule 
