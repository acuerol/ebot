module esp #(
	parameter clk_freq = 50000000
)(
	input clk,
	input rst,
	input mod_rst,
	output reg rst_esp
);	
   parameter delay = clk_freq/1000;

   //States
   parameter START = 0;
   parameter COUNT = 1;
   
   reg state;
   
   wire busy;
   
   assign busy = state;
   
   reg [35:0] counter;
	
	always@(posedge clk) begin
	   if(rst) begin
			state = START;
		end else begin
	      case (state)
	         START : begin
	            rst_esp = 1;
	            counter = 0;
               if(mod_rst == 0)
	               state = COUNT;
               else
                  state = START;
	         end
	         COUNT : begin
	            rst_esp = 0;
	            
	            counter = counter + 1;
	            if(counter < delay)
	               state = COUNT;
	            else
	               state = START;
	         end
	         default : begin
	            rst_esp = 1;
	            counter = 0;
	         end
	      endcase
      end
	end
endmodule
