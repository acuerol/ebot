module trigger #(
	parameter clk_freq = 50000000
)(
	input clk,
	input rst,
	input enabler,
	
	output reg trigger
);

   reg [15:0] counter;
   
   parameter FREQ = clk_freq/100000;
//   parameter FREQ = 100;
   
   initial begin
      trigger = 0;
      counter = 0;
   end

   // States
   parameter START = 0;
   parameter TRIGGER = 1;

   reg state;

   always@ (posedge clk) begin
	   if(rst) begin
		   state = START;
		   counter = 0;
		   trigger = 0;
	   end else begin
	      case(state)
	      START: begin
	         trigger = 0;
	         counter = 0;
	         if(enabler)
	            state = TRIGGER;
	         else
	            state = START;
	      end
	      TRIGGER: begin
	         trigger = 1;
	         counter = counter + 1;
	         if(counter > FREQ)
	            state = START;
	         else
	            state = TRIGGER;
	      end
	         default:
	            state = START;
	      endcase
      end
   end
endmodule
