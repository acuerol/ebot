module wb_motor #(
	parameter clk_freq = 50000000,
	parameter pwm_freq = 250
)(
	input clk,
	input rst,
	
	input [31:0]wb_adr_i,
	input [31:0]wb_dat_i,
	output reg [31:0]wb_dat_o,
	input wb_stb_i,
	input wb_cyc_i,
	input wb_we_i,
	input [3:0]wb_sel_i,
	output wb_ack_o,
	//Pins
	output pwm_forward,
	output pwm_backward
);

	reg ack;
	reg [6:0] speed;
	reg way;
	
	assign wb_ack_o = wb_stb_i & wb_cyc_i & ack;
	
	wire wb_rd = wb_stb_i & wb_cyc_i & ~wb_we_i;
	wire wb_wr = wb_stb_i & wb_cyc_i &  wb_we_i;
	
	pwm #(
		.clk_freq(clk_freq),
		.pwm_freq(pwm_freq)
	) pwm0(
		.clk(clk),
		.rst(rst),
		.speed(speed),
		.way(way),
		.pwm_forward(pwm_forward),
		.pwm_backward(pwm_backward)
	);
	
	always @(posedge clk) begin 
		if (rst) begin
			wb_dat_o <= 32'b0;
			ack    <= 0;
			speed <= 7'b0;
			way <= 0;
		end else begin
			wb_dat_o <= 32'b0;
			ack    <= 0;
			speed <= speed;
			way <= way;
			if(wb_wr & ~ack) begin
				ack <= 1;
				case(wb_adr_i[1:0])
					2'b0: way <= wb_dat_i[0];
					2'b10: speed <= wb_dat_i[6:0];
					default: begin
						speed <= 7'b0;
						way <= 0;
					end
				endcase
			end 
		end
	end

endmodule
